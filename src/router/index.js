import { createRouter, createWebHistory } from 'vue-router'
import main from '@/router/main'
import company from '@/router/company'
import consulting from '@/router/consulting'
import solution from '@/router/solution'
import contract from '@/router/contract'

const routes = [
  ...main,
  ...company,
  ...consulting,
  ...solution,
  ...contract,
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
