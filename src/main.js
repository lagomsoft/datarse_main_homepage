import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueGtag from 'vue-gtag'

import '@/common/css/reset.css'
import '@/common/css/notosanskr.css'
import '@/common/css/common.css'
import '@/common/font-awesome/css/font-awesome.css'
//const app = createApp(App)

//app.use(router).mount('#app')

createApp(App)
    .use(router)
    .use(VueGtag, {
        config: {
            id: 'G-JKGYT3KFV3'
        }
    })
    .mount('#app')